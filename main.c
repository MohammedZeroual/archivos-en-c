#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#define str(x) #x
#define xstr(x) str(x)
#define MAX_NOM 15
#define MAX_TIPUS 35
#define NOM_FITXER "animals.dat"
#define BB while(getchar()!='\n')
#define ERROR_ABRIR_ARCHIVO "\tError al abrir el archivo"
#define ERROR_ESCRITURA_ARCHIVO "\tError de escritura al archivo"
#define ERROR_LECTURA_ARCHIVO "\tError de lectura del archivo"
#define OPERACION_CANCELADA "\tOperacion cancelada por el usuario"
#define ERROR_NO_PUEDE_BORRAR_ARCHIVO "\tError  al abrir el archivo"
#define ERROR_CAMBIAR_NOMBRE_ARCHIVO "\tError al renombrar el archivo"
#define INFORMACION_ERRORES "\tTodo Correcto!"

//menú
#define LINEAS "\t\t**********************\n"
#define ALTA "\t\t1  - Alta\n"
#define BAJA "\t\t2  - Baja\n"
#define CONSULTAR "\t\t3  - Consultar\n"
#define MODIFICAR "\t\t4  - Modificar\n"
#define CONSULTAR_BORRADOS "\t\t5  - Consultar Borrados\n"
#define BORRAR_ARCHIVO "\t\t6  - Borrar Archivo\n"
#define COMPACTAR_ARCHIVO "\t\t7  - Compactar Archivo\n"
#define ACCESO_DIRETO "\t\t8  - Acceso Directo\n"
#define INFORME "\t\t10 - Informe\n"
#define SALIR "\n\t\t0 - Sortir\n\n"
//Teclas
#define PULSAR "\n\tPulsa una tecla para continuar "
//AVISOS
#define SEGUIR_BAJA "\n\tSeguro que quieres modificar este registro(s/n): "
//Introducir Datos
#define PONER_NOMBRE "\n Introduce el Nombre: "
#define PONER_TIPOS "\n Introduce el Tipos: "
#define PONER_SEXO  "\n Introduce el sexo( f=>Femenino / m=>Masculino): "
#define PONER_EDAD "\n Introduce la edad: "
#define PONER_PESO "\n Introduce el peso: "
// Windows
#ifdef __WIN32
    #define MENU "\t\t******** MEN%c ********\n"
    #define NUMERO_REGISTROS "\t\t9  - N%cmero De Registros\n"
    #define OPCION "\tElige Opci%cn: (0-7)"
    #define BORRADO "\n\tEst%c Borrado!"
    #define EN_PELIGRO "\n Est%c en peligro de extinci%cn (s/n)?"
    #define SI_EN_PELIGRO "\n Est%c en peligro\n\n"
    #define NO_EN_PELIGRO "\n No Est%c en peligro\n\n"
#endif // __WIN32
//Linux
#ifdef __linux__
    #define MENU "\t\t******** MENÚ ********\n"
    #define NUMERO_REGISTROS "\t\t9  - Número De Registros\n"
    #define OPCION "\tElige Opciócn: (0-7)"
    #define BORRADO "\n\tEstá Borrado!"
    #define EN_PELIGRO "\nEstá en peligro de extinción (s/n)?"
    #define SI_EN_PELIGRO "\n Está en peligro\n\n"
    #define NO_EN_PELIGRO "\n No Está en peligro\n\n"

#endif // __linux__

typedef struct{
	char tipus[MAX_TIPUS+1];
	char nom[MAX_NOM+1];
	char sexe;// m-masculi, f-femeni
	bool enPerillExtincio;
	int edat;
	double pes;
	char marca;
} Animal; //estructura que representa el fitxer intern.

void escriureAnimal(Animal perso);
void entrarAnimal(Animal *animal,bool modifica);
void entraOpcio(int *opcio);
void pintarMenu();
int alta(char nomFitxer[]);
int consulta(char nomFitxer[],bool borrado);
int  baixaModifica(char nomFitxer[],bool modifica);
bool seguirBaixaModifica(bool modifica);
int consultaModificat(char nomFitxer[]);
int borrar(char fitxer[]);
int compactar(char nomfitxer[]);
int numeroRegistros(char fitxer[]);
int acceso(char fitxer[]);
int informe(char fitxer[]);
int main()
{
    int opcio,error;
    do{
        pintarMenu();
        entraOpcio(&opcio);
        error=0;
        switch(opcio){
        case 1:
            error=alta(NOM_FITXER);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-2) printf(ERROR_ESCRITURA_ARCHIVO);
            break;
        case 2:
            error=baixaModifica(NOM_FITXER,false);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-2) printf(ERROR_ESCRITURA_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            if(error==-4) printf(OPERACION_CANCELADA);
            break;
        case 3:
            error=consulta(NOM_FITXER,false);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-2) printf(ERROR_ESCRITURA_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            break;
        case 4:
            error=baixaModifica(NOM_FITXER,true);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-2) printf(ERROR_ESCRITURA_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            break;
        case 5:
            error=consulta(NOM_FITXER,true);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-2) printf(ERROR_ESCRITURA_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            break;
        case 6:
            error=borrar(NOM_FITXER);
            if(error!=0) printf(ERROR_NO_PUEDE_BORRAR_ARCHIVO);
            break;

        case 7:
            error=compactar(NOM_FITXER);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            if(error==-5) printf(ERROR_CAMBIAR_NOMBRE_ARCHIVO);
            if(error==0){
                printf(INFORMACION_ERRORES);
                getchar();
                }
            break;
        case 8:
            error=acceso(NOM_FITXER);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            break;
        case 9:
            error=numeroRegistros(NOM_FITXER);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);
            if(error==-3) printf(ERROR_LECTURA_ARCHIVO);
            break;
        case 10:
            error=informe(NOM_FITXER);
            if(error==-1) printf(ERROR_ABRIR_ARCHIVO);

            break;
        }
    if(error!=0){
        printf(PULSAR);
        getchar();
    }
    }while(opcio!=0);

    return 0;
}



void entraOpcio(int *opcio){
    scanf("%d",opcio);BB;
}

void pintarMenu(){
    system("cls || clear");
    printf(LINEAS);
    printf(MENU,233);
    printf(LINEAS);
    printf(ALTA);
    printf(BAJA);
    printf(CONSULTAR);
    printf(MODIFICAR);
    printf(CONSULTAR_BORRADOS);
    printf(BORRAR_ARCHIVO);
    printf(COMPACTAR_ARCHIVO);
    printf(ACCESO_DIRETO);
    printf(NUMERO_REGISTROS,163);
    printf(INFORME);
    printf(SALIR);
    printf(OPCION,162);
}
int informe(char fitxer[]){
    FILE *A,*B;
    Animal a;
    char inciohtml[]="<!DOCTYPE html>\n\
<html lang='es'>\n\
    <head>\n\
    \t<meta charset='UTF-8'>\n\
    \t<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n\
    \t<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n\
    \t<title>Informe</title>\n\
    \t<link rel='stylesheet' href='estilos.css'>\n\
    </head>\n\
    <body class='app'>\n\
    \t<header class='header'> <h1>Informe</h1> </header>\n\
    \t<aside class='aside'>\n\
    \t\t<table>\n\
    \t\t\t<tr>\n\t\t\t<th>Nombre</th>\n\t\t\t<th>Tipos</th>\n\t\t\t<th>Sexo</th>\n\t\t\t<th>Edad</th>\n\t\t\t<th>Peso</th>\n\t\t\t<th>Estado</th>\n\t\t</tr>";
    char finalhtml[]="</table>\n\
    </side>\n\
    <footer class='footer'></footer>\n\
\t</body>\n\
</html>\n ";
    char todo[180]=" ";
    char cadena[80];
    int n;
    A=fopen(fitxer,"r");
    if(A==NULL){
        return -1;
    }
    B=fopen("informe.html","w");
    if(B==NULL){
        return -1;
    }
    fputs(inciohtml,B);
    while(!feof(A)){
        n=fread(&a,sizeof(Animal),1,A);
        if(!feof(A)){
                if(n==0){
                    return -3;
                }
        if(a.marca!='*'){
                todo[0]='\0';
                strcat(todo,"\n\t<tr>");
                    strcat(todo,"\n\t\t<td>");
                    strcat(todo,a.nom);
                    strcat(todo,"\t</td>\n");
                    strcat(todo,"\n\t\t<td>");
                    strcat(todo,a.tipus);
                    strcat(todo,"\t</td>\n");
                    strcat(todo,"\n\t\t<td>");
                    if(a.sexe=='m') strcpy(cadena,"Masculino");
                        else strcpy(cadena,"Femenino");
                    strcat(todo,cadena);
                    strcat(todo,"\t</td>\n");
                    strcat(todo,"\n\t\t<td>");
                    sprintf(cadena,"%d",a.edat);
                    strcat(todo,cadena);
                    strcat(todo,"\t</td>\n");
                    strcat(todo,"\n\t\t<td>");
                    sprintf(cadena,"%.2lf",a.pes);
                    strcat(todo,cadena);
                    strcat(todo,"\t</td>\n");
                    strcat(todo,"\n\t\t<td>");
                    if(a.enPerillExtincio) strcpy(cadena,"Esta En Peligro");
                        else strcpy(cadena,"No Esta En Peligro");
                    strcat(todo,cadena);
                    strcat(todo,"\t</td>\n");
                strcat(todo,"\n\t</tr>\n");
                fputs(todo,B);

            }

        }
    }
    fputs(finalhtml,B);
    fclose(A);
    fclose(B);
    #ifdef __linux__
        system("firefox informe.html");
    #endif // __linux__
    #ifdef __WIN32
        printf("\n\tSe ha Generado Correctamente infrome.html");
        printf(PULSAR);
        getchar();
    #endif // __WIN32
    return 0;
}
int acceso(char fitxer[]){
    int posicion,n,r,e;
    Animal a;
    FILE *origen;
    r=numeroRegistros(NOM_FITXER);
    origen=fopen(fitxer,"rb");
    if( origen == NULL ) {
        return -1;
    }
    do{
        printf("\tPon el N del registro: ");
        scanf("%d",&posicion);BB;
    }while(posicion<1 || posicion>r);
    n=posicion-1;
    fseek(origen,(n*sizeof(Animal)),SEEK_SET);
    e=fread(&a,sizeof(Animal),1,origen);
    if(e==0){
        return -3;
    }
    if(a.marca!='*'){
        escriureAnimal(a);
    }else printf(BORRADO,160);
    printf(PULSAR);
    getchar();
    fclose(origen);
    return 0;
}
int numeroRegistros(char fitxer[]){
    long numero,n;
    Animal a;
    FILE *origen;
    origen=fopen(fitxer,"rb");
    if( origen == NULL ) {
        return -1;
    }
    n=fread(&a,sizeof(Animal),1,origen);
    if(n==0){
        return -3;
    }
    if(a.marca!='*'){
            fseek(origen,(long)0L,SEEK_END);
            numero= ftell(origen);
            numero= numero/sizeof(Animal);
        }
    fclose(origen);
    printf("\n\tHay %ld registros %s\n",numero,fitxer);
    return numero;
}
int compactar(char nomfitxer[]){
 Animal a;
 FILE *origen,*desti;
 int n;
 origen=fopen(nomfitxer,"rb");
    if(origen==NULL) {
        return -1;
    }
 desti=fopen("tmp.dat","wb");
  if(desti==NULL ) {
        return -1;
    }
    while(!feof(origen)){
        n=fread(&a,sizeof(Animal),1,origen);
        if(!feof(origen)){
            if(n==0){
                return -3;
                }
            if(a.marca!='*'){
                n=fwrite(&a,sizeof(Animal),1,desti);
            }
        }
    }
    fclose(origen);
    fclose(desti);
    borrar(nomfitxer);
    if(rename("tmp.dat",nomfitxer)==-1){
        return -5;
    }
    return 0;
}
int alta(char nomFitxer[]){
    Animal a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"ab");
    if( f1 == NULL ) {
        return -1;
    }
    entrarAnimal(&a1,false);
    n= fwrite(&a1,sizeof(Animal),1,f1);
    if(n==0) {
        return -2;
    }
    fclose(f1);
    return 0;
}

int consulta(char nomFitxer[],bool borrado){
    Animal a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"rb");
    if( f1 == NULL ) {
        return -1;
    }
    system("cls || clear");
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal), 1,f1);
        if(!feof(f1)){
            if(n==0){
                return -3;
            }
            if(!borrado){
                if(a1.marca!='*'){
                    escriureAnimal(a1);
                }
            }else{
                if(a1.marca=='*'){
                    escriureAnimal(a1);
                }
            }
        }
    }
    fclose(f1);
    printf(PULSAR);
    getchar();
    return 0;

}
bool seguirBaixaModifica(bool modifica){
    char opcio;
    bool seguir=false;

      if(!modifica){
            do{
                    printf(SEGUIR_BAJA);
                    scanf("%c",&opcio);BB;
            }while(opcio!='s' && opcio!='n');
        }
       if(modifica){
           do{
            printf(SEGUIR_BAJA);
            scanf("%c",&opcio);BB;
            }while(opcio!='s' && opcio!='n');
        }
        if(opcio=='s') seguir=true;

        return seguir;
}
/* bool modifica : si es true indica que quiere hacer una modificacion,
si es false significa que quiere hacer una baja*/
int  baixaModifica(char nomFitxer[],bool modifica){
    Animal a1;
    FILE *f1;
    int n;
    char nom[MAX_NOM];
    printf(PONER_NOMBRE);
    scanf("%"xstr(MAX_NOM)"[^\n]",nom);BB;
    f1=fopen(nomFitxer,"rb+");
    if(f1== NULL) {
        return -1;
    }
    while(!feof(f1)){
            n=fread(&a1,sizeof(Animal),1,f1);
        //if((n=fread(&a1,sizeof(Animal),1,f1))!=0){ Tambien funciona pero puede fallar
            if(!feof(f1)){
                    if(n==0) {
                            return -2;
                        }
                    if( a1.marca!='*' && strcmp(a1.nom,nom)==0 ){
                    escriureAnimal(a1);
                    if(!seguirBaixaModifica(modifica)) return -4;
                    if(seguirBaixaModifica(modifica)){
                        if(fseek(f1,-(long) sizeof(Animal),SEEK_CUR )){ return -2;}
                            if(!modifica){
                            a1.marca='*';
                            n=fwrite(&a1,sizeof(Animal),1,f1);
                            } else
                                if(modifica){
                                   entrarAnimal(&a1,modifica);
                                   n=fwrite(&a1,sizeof(Animal),1,f1);
                                }
                            if(n!=1){
                                return -3;
                            }
                            break;
                        }
                    }
                }
            }
    fclose(f1);
    return 0;
}

int borrar(char fitxer[]){
    return unlink(fitxer);
}

void entrarAnimal(Animal *animal,bool modifica){
	char enPerill;
	int correcto;
	if(!modifica){
        printf(PONER_NOMBRE);
        scanf("%"xstr(MAX_NOM)"[^\n]",animal->nom);BB;
    }
    printf(PONER_TIPOS);
    scanf("%"xstr(MAX_TIPUS)"[^\n]",animal->tipus);BB;
    do{
        printf(PONER_SEXO);
        scanf("%c",&animal->sexe);BB;
    }while(animal->sexe!='m' && animal->sexe!='f');
    do{
        printf(PONER_EDAD);
        correcto=scanf("%d",&animal->edat);BB;
    }while(correcto==0);
    do{
        printf(PONER_PESO);
        correcto=scanf("%lf",&animal->pes);BB;
    }while(correcto==0);
    do{
        printf(EN_PELIGRO,160,162);
        scanf("%c",&enPerill);BB;
    }while(enPerill!='s' && enPerill!='n');
    if(enPerill=='s') animal->enPerillExtincio=true;
    else animal->enPerillExtincio=false;

}
void escriureAnimal(Animal animal){
    printf("\n el Nombre: %s",animal.nom);
    printf("\n el tipos: %s",animal.tipus);
    printf("\n el sexo: %c",animal.sexe);
    printf("\n la edad: %d",animal.edat);
    printf("\n el peso: %lf",animal.pes);
    if(animal.enPerillExtincio==true) printf(SI_EN_PELIGRO,160) ;
    else printf(NO_EN_PELIGRO,160);
}
