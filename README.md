# Gestion De Archivos #
## M-3  Programacion | UF-3 Fonaments de gestió de fitxers | 09/02/2021 ##
---
### Este programa esta hecho con el Lenguaje C con el programa CodeBlocks, en este programa se hace un crud ( Create,Read,Update and Delete) de la siguiente Estructura: ###
    typedef struct{
        char tipus[MAX_TIPUS+1];
        char nom[MAX_NOM+1];
        char sexe;// m-masculi, f-femeni
        bool enPerillExtincio;
        int edat;
        double pes;
        char marca;
    } Animal;
    El nom es la ID de la estructura.
----
* Se ha creado un menu que tiene las opcions de 0 al 10, el 0 es la opcion para salir del programa mientras que las otras tienen cada una su funcion.
    ----
    ![menu](imagenes/menu.png)
    ----  ----
1. La opcion 1 llamada Alta , se encarga de cear los registros que van den el archivo animales.dat.
    ----

    ![Alta](imagenes/1.png)
    ----  ----

2. La opcion 2 llamada baja, se encarga de dar baja a un registro, no lo elimina sino que se le aplica un filtro (*) que lo que hace es cuando se hace una consulta no se vea.
    ----
    ![Baixa](imagenes/2.png)
    ----  ----

3. La opcion 3 llamada Consultar, se encarga de mostrar todos lo registros, no muestra los que estan en baja.
    ----
    ![Consulta](imagenes/3.png)
    ----  ----

4. La opcion 4 llamada Modificar, se encarga de modificar un registro a partir de su nombre o ID.
    ----
    ![Modificar](imagenes/4.png)
    ----  ----

5. La opcion 5 llamada Consultar Borrados, se encarga de consultar los registros borrados, los que tienen el filtro solo(*).
    ----
    ![ConsultarBorrados](imagenes/5.png)
    ----  ----

6. La opcion 6 llamada Borrar Archivo, se encarga de eliminr el archivo.
    ----
    ![BorrarArchivo](imagenes/6.png)
* una Vez eliminado, ya no se puede hacer nada ya que no existe el archivo, se puede hacer la alta ya que crea de nuevo el archivo pero sin nada.
    ![BorrarArchivo](imagenes/6_1.png)
    ----  ----

7. La opcion 7 llamada Compactar Archivo, se encarga de compactar el archivo para que el archivo final no tenga los registros borrar (los que tienen el filtro(*)).
    ----
    ![CompactarArchivo](imagenes/7.png)
    ----  ----

8. La opcion 8 llamada Acecco Directo, se encarga que apartir de un registro que un usuario introduce, lo busca y lo muestra.
    ----
    ![AceccoDirecto](imagenes/8.png)
    ----  ----

9. La opcion 9 llamada Numero de registros, se encarga de mostrar todos los registros que tiene el archivo.

    ----
    ![NumeroDeRegistros](imagenes/9.png)
    ----  ----

10. La opcion 10 llamada Informe, se encarga de generar un informe .html con todos lo registros que no tengan la marca de borrar.

    ----
    ![Informe](imagenes/10_1.png)
    ----  ----

* Una vez Generado el informe y como esta hecho con la forma de una tabla sale de la siguiente manera :
    ----
    ![Informe](imagenes/10_2.png)
    ----  ----
